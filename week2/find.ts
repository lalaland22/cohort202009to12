class Shape{

    constructor(name){
        console.log("You've made a "+ name +".\n")
    }

    circleArea(radius){
    var area= Math.PI * Math.pow(radius,2) 
    console.log('AREA:'+ area)
    }
    triangleArea(base,height){
        var area = 0.5* base * height
        console.log('AREA:' + area)
        }
    squareArea(length){
        var area = Math.pow(length,2)
        console.log('AREA:'+ area)     
    }
}

class Silhouette extends Shape{
    
    }
 var newShape = new Silhouette ('Square')
 newShape.squareArea(5)
